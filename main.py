import argparse
from asyncio.windows_events import NULL
import sys
from unicodedata import name
import emoji
from tkinter import *
import random, string
import pyperclip
from progress.bar import Bar
from colorama import Fore, Back, Style
import typer


app = typer.Typer()
title = emoji.emojize("Gitlab Cli Tool :red_heart::red_heart::red_heart:")
pass_len = NULL
pass_str = NULL

SYSTEMVERSION = (
    str(sys.version_info.major)
    + "."
    + str(sys.version_info.minor)
    + "."
    + str(sys.version_info.micro)
)


@app.command()
def mergerequest(sourceBranch: str, targetBranch: str = "main"):
    print(mergerequest)


@app.command()
def sayMyName2():
    print("Niklas")


def generator():
    password = ""

    for x in range(0, 4):
        password = (
            random.choice(string.ascii_uppercase)
            + random.choice(string.ascii_lowercase)
            + random.choice(string.digits)
            + random.choice(string.punctuation)
        )
    for y in range(pass_len.get() - 4):
        password = password + random.choice(
            string.ascii_uppercase
            + string.ascii_lowercase
            + string.digits
            + string.punctuation
        )
    pass_str.set(password)


def copy_password():
    pyperclip.copy(pass_str.get())


def startGUI():
    root = Tk()
    root.geometry("500x500")
    root.resizable(0, 0)
    root.title("Gitlab CLI")
    Label(root, text="PASSWORD GENERATOR", font="arial 15 bold").pack()
    Label(root, text="Gitlab CI", font="arial 15 bold").pack(side=BOTTOM)
    pass_label = Label(root, text="PASSWORD LENGTH", font="arial 10 bold").pack()
    global pass_len, pass_str
    pass_len = IntVar()
    pass_str = StringVar()
    length = Spinbox(root, from_=8, to_=32, textvariable=pass_len, width=15).pack()
    Entry(root, textvariable=pass_str).pack()
    Button(root, text="GENERATE PASSWORD", command=generator).pack(pady=5)
    Button(root, text="COPY TO CLIPBOARD", command=copy_password).pack(pady=5)
    root.mainloop()


def start():
    bar = Bar("Processing", max=20000)
    for i in range(20000):
        bar.next()
    bar.finish()


def main():
    if sys.version_info > (2, 7):
        sys.getsizeof(SYSTEMVERSION)
        print(emoji.emojize(f"Python {SYSTEMVERSION}"))
        start()
    else:
        print(emoji.emojize("Please use python3. :snake:"))


if __name__ == "__main__":
    print(Fore.GREEN)
    print(title.title())
    main()
    print(Style.RESET_ALL)
    app()
